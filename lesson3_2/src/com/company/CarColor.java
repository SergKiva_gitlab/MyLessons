package com.company;

public enum CarColor {
    YELLOW(255,255,0),
    PINK(255,192,203),
    AZURE(0,127,255);

    private int red;
    private int green;
    private int blue;

    CarColor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

}
