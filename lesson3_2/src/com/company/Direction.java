package com.company;

public enum Direction {

    LEFT(1, "LEFT++"),
    RIGHT(2,"RIGHT++"),
    UP(3,"UP++"),
    DOWN(4,"DOWN++");

    int value;
    String name;

    Direction(int value, String name) {
        this.value = value;
        this.name = name;
    }
}
