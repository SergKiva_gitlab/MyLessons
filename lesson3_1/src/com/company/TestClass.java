package com.company;

public class TestClass {
    private int test;

    public TestClass(int test) {
        this.test = test;
    }

    public TestClass() {

    }

    public int getTest() {
        return test;
    }

    public void setTest(int test) {
        this.test = test;
    }
}
