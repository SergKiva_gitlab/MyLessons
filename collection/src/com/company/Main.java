package com.company;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private static Map<Integer, List<Article>> articleMap = new HashMap<>();

    public static void main(String[] args) {

//        ReadValue readValue = new ReadValue();
//        int iCount = readValue.getReadedNumber("Size of list:");
//
//        List<Car> list = new ArrayList<>();
//        String nameCar;
//        for (int i=0; i < iCount; i++) {
//             nameCar = readValue.getInputString("Type of car:");
//             list.add(new Car(nameCar));
//        }
//
//        System.out.println("Size of list:" + list.size());
//
////        for (Car myCar : list) {
////            myCar.printCar();
////        }
//
//
//        list.forEach(fCar -> fCar.printCar());
//
//        List<String> models =  list.stream()
//                .filter(item -> item.getMark().length() > 4)
//                .map(item -> item.getMark())
//                .collect(Collectors.toList());
//
//        System.out.println("\nModels!");
//        models.forEach(model -> System.out.println(model));
//
//        Car testCar = new Car("Jaguar");
//        System.out.println("Is testCar in list: " + list.contains(testCar));

//        Map<String, Box>  boxMap = new HashMap<>();
//        Box box1 = new Box();
//        box1.setApples(5);
//
//        Box box2 = new Box();
//        box1.setApples(2);
//        Box box3 = new Box();
//        box1.setApples(7);


//        List<Box> boxList = ArrayList<>();
//        boxList.add(box1);
//        boxList.add(box2);
//        boxList.add(box3);

//        boxMap.put("Kalenchik", box1);
////        boxMap.put("Tokarev", box2);
//        boxMap.put("Lyahor", box3);
//
//        System.out.println(boxMap.get("Lyahor").getApples());
//
//        // method 1
//        if ( boxMap.get("Tokarev") == null ) {
//            boxMap.put("Tokarev", box2);
//        }
//
//        // method 2
//        if ( ! boxMap.containsKey("Tokarev")) {
//            boxMap.put("Tokarev", box2);
//        }
//
//
//        if ( ! boxMap.containsValue(box2) ) {
//            boxMap.put("Tokarev", box2);
//        }
//
//        boxMap.putIfAbsent("Tokarev", box2);

//        Map<Integer, Article> articleMap  = new HashMap<>();
//          10


        Random rand = new Random();
        int count = rand.nextInt(10);
        if (count < 2) {
            count = rand.nextInt(10) + 2;
        }
        System.out.println("Count: " + count);

        for (int i = 0; i < count; i++) {
            Random rand2 = new Random();
            Integer page = rand2.nextInt(3);
            if (page == 0) {
                page = 3;
            }
            System.out.println("generate page number:" + page.toString());
            System.out.println(getArticles(page));

        }

    }

    // page from 1 to 3
    public static List<Article> getArticles(Integer page) {
        List<Article> articles = new ArrayList<>();

        if (articleMap.containsKey(page) ) {
            return articleMap.get(page);
        }

        if (page == 1) {
            System.out.println("Get new articles for page 1");
            articles = new ArrayList<>();
            articles.add(new Article("1"));
            articles.add(new Article("2"));
            articles.add(new Article("3"));


        } else  if (page == 2) {
            System.out.println("Get new articles for page 2");
            articles = new ArrayList<>();
            articles.add(new Article("4"));
            articles.add(new Article("3"));
            articles.add(new Article("5"));

        } else  if (page == 3) {
            System.out.println("Get new articles for page 3");
            articles = new ArrayList<>();
            articles.add(new Article("6"));
            articles.add(new Article("7"));
            articles.add(new Article("8"));

        }

        articleMap.putIfAbsent(page, articles);

        return articles;
    }
}
