package com.company;

public class Article {
    private String title;

    public Article(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "article number '" + title;
    }
}
