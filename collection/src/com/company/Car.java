package com.company;

import java.util.Objects;

public class Car {
    private String mark;

    public Car(String mark) {
        this.mark = mark;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(mark, car.mark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mark);
    }

    public void printCar() {
        System.out.println("Type:" + mark);
    }
}
