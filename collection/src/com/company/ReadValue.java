package com.company;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class ReadValue {

    public int getReadedNumber(String sayInput)  throws IOException {
        int readedInt;

        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        System.out.println(sayInput);
        try {
            readedInt = Integer.parseInt(bufferedReader.readLine());
        } catch(NumberFormatException nfe) {
            readedInt = (int) (Math.random() * 5);;
            System.err.println("Invalid Format! Set random value = " + readedInt);
        }
        return readedInt;
    }

    public int randomValue() {
        return (int) (Math.random() * 11 ) + 10;
    }

    public String randomStringValue() {
        int len = (int) (Math.random() * 11 ) + 10;
        byte charcode;
        char[] c = new char[len];
        for ( int i=0; i < len; i++ ) {
            charcode = (byte) ((Math.random() * 26 ) + 65);
            c[i] = (char) charcode;
        }
        return new String(c);
    }

    public String getInputString(String sayInput) throws IOException {

        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        System.out.println(sayInput);
        String s;
        try {
            s = bufferedReader.readLine();
        } catch(NumberFormatException nfe) {
            s = randomStringValue();
            System.err.println("Invalid string! Set random value = " + s);
        }
        return s;
    }
}
