package com.company;

import java.util.Objects;

public class Box {
    private int apples;

    public int getApples() {
        return apples;
    }

    public void setApples(int apples) {
        this.apples = apples;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Box box = (Box) o;
        return apples == box.apples;
    }

    @Override
    public int hashCode() {
        return Objects.hash(apples);
    }
}
