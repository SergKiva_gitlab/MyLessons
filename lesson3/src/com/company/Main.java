package com.company;

public class Main {

    public static void main(String[] args) {

        Car abc = new Car();
        abc.color = "White";
        abc.countOfDoors = 4;
        abc.passengersCount = 3;

        if (abc.passengersCount > 2 && abc.countOfDoors == 4) {
            System.out.println("greater then 2");
        } else if (abc.passengersCount > 0) {
            System.out.println("less then 3 and greater than zero");
        } else {
            System.out.println("less then 1");

        }

        int a = abc.passengersCount>1 ? 1 : 2;

        switch(abc.passengersCount) {
            case 1:
                System.out.println("1");
                break;
            case 2:
                System.out.println("2");
                break;
            default:
                System.out.println("error");


        }

        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }


        int i = 5;
        do {
            System.out.println(i);
        } while (i > 0);

        i = 5;
        while (i > 0) {
            System.out.println(i);
            i--;
        }

        int[] array = new int[3];
        array[0] = 2;
        array[1] = 34;
        array[2] = 46667;

        for (int tmp : array) {
            System.out.println(tmp);
        }



        JamesBondCar car2 = new JamesBondCar();
        car2.color = "Black";
        car2.countOfDoors = 2;
        car2.gun = true;

        abc.startEngine();
        car2.startEngine();

	    // System.out.println("Hello world!!!");
    }
}
